﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3
{

    class Customer
    {
        public string name;
        public string phone;

        public Customer(string _name, string _phone)
        {
            name = _name;
            phone = _phone;
        }

        public void Print()
        {
            Console.WriteLine($"Name: {name}");
            Console.WriteLine($"Phone: {phone}");
        }

    }

    class Pizza
    {
        public void PrintPizza()
        {
            var pizza = new List<string> { "1) Meatlovers", "2) Hawaiian", "3) Pepperoni" };
            foreach(var x in pizza)
            {
                Console.WriteLine(x);
            }
            
        }
        public void HawaiianPrice()
        {
            int small = 5;
            int large = 10;

            Console.WriteLine($"Small: ${small}");
            Console.WriteLine($"Large: ${large}");

        }
        public void MeatloversPrice()
        {
            int small = 8;
            int large = 12;

            Console.WriteLine($"Small: ${small}");
            Console.WriteLine($"Large: ${large}");

        }
        public void PepperoniPrice()
        {
            int small = 5;
            int large = 8;

            Console.WriteLine($"Small: ${small}");
            Console.WriteLine($"Large: ${large}");

        }
    }

    class Drinks
    {
        public void DrinkType()
        {
            var drinks = new List<string> { "1) Coke", "2) Coke Zero", "3) Juice" };
            foreach (var x in drinks)
            {
                Console.WriteLine(x);
            }

        }
        public void Coke()
        {
            double price = 2.50;
            Console.WriteLine($"Coke: ${price} ea.");

        }
        public void CokeZero()
        {
            double price = 2.50;
            Console.WriteLine($"Coke Zero: ${price} ea.");

        }
        public void Juice()
        {
            double price = 3.50;
            Console.WriteLine($"Juice: ${price} ea.");

        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine($"Welcome to Sam's Pizza, Please Enter your name and phone number");
            string name = CustomerName();
            string phone = CustomerPhone();
            Customer newcust = new Customer(name, phone);

            Order();

            Console.WriteLine($"Would you like to add drinks to your order. Y/N");
            var select = "";
            select = Console.ReadLine();
            select = select.ToLower();
            if (select == "y")
            {
                Drinkorder();
            }

            newcust.Print();
            Checkdetail();
            Payment();

            Console.WriteLine("Order Summary:");
            newcust.Print();
            foreach (var x in order)
            {
                Console.WriteLine($"{x.Key}, {x.Value}");
            }
            foreach (var x in drink)
            {
                Console.WriteLine($"{x.Item1}, {x.Item2}");
            }
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        public static string CustomerName()
        {
            string name = "";

            Console.WriteLine($"Please enter your name");
            name = Console.ReadLine();

            return name;

        }

        public static string CustomerPhone()
        {
            string phone = "";

            Console.WriteLine($"Please enter your phone number");
            phone = Console.ReadLine();
            Console.Clear();
            return phone;
        }

        public static Dictionary<string, int> order = new Dictionary<string, int>();
        public static List<Tuple<string, double>> drink = new List<Tuple<string, double>>();

        public static Dictionary<string, int> Order()
        {
            
            var id = 0;
            var select = "y";
            do
            {
                Pizza pizzalist = new Pizza();
                pizzalist.PrintPizza();
                
                Console.WriteLine($"Please 1 to add Meatlovers, 2 to add Hawaiian or 3 to add Pepperoni");
                string idchk = Console.ReadLine();
                int x;
                while (!Int32.TryParse(idchk, out x))
                {
                    Console.WriteLine("Not a valid option, please enter 1, 2 or 3.");

                    idchk = Console.ReadLine();
                }
                id = x;
                Console.Clear();
                if (id == 1)
                {
                    Pizza price = new Pizza();
                    price.MeatloversPrice();
                    Console.WriteLine("Please enter small to order a small pizza or large to order a large pizza");
                    var size = Console.ReadLine();
                    size = size.ToLower();
                    Console.Clear();
                    if (size == "small")
                    {
                        var smallprice = 8;
                        order.Add("Meatlovers", smallprice);
                        Console.WriteLine("Do you want to order another pizza? Y/N");
                        select = Console.ReadLine();
                        select = select.ToLower();
                        Console.Clear();
                    }
                    if (size == "large")
                    {
                        var lgeprice = 12;
                        order.Add("Meatlovers", lgeprice);
                        Console.WriteLine("Do you want to order another pizza? Y/N");
                        select = Console.ReadLine();
                        select = select.ToLower();
                        Console.Clear();
                    }
                    if ((size != "small") && (size != "large"))
                    {
                        Console.WriteLine("You need to enter small or large when choosing size. Please try again");
                        id = 1;
                    }
                  }
                    if (id == 2)
                    {
                        Pizza price = new Pizza();
                        price.HawaiianPrice();
                        Console.WriteLine("Please enter small to order a small pizza or large to order a large pizza");
                        var size = Console.ReadLine();
                        size = size.ToLower();
                        Console.Clear();
                        if (size == "small")
                        {
                            var smallprice = 5;
                            order.Add("Hawaiian", smallprice);
                            Console.WriteLine("Do you want to order another pizza? Y/N");
                            select = Console.ReadLine();
                            select = select.ToLower();
                            Console.Clear();
                        }
                        if (size == "large")
                        {
                            var lgeprice = 10;
                            order.Add("Hawaiian", lgeprice);
                            Console.WriteLine("Do you want to order another pizza? Y/N");
                            select = Console.ReadLine();
                            select = select.ToLower();
                            Console.Clear();
                        }
                        if ((size != "small") && (size != "large"))
                        {
                            Console.WriteLine("You need to enter small or large when choosing size. Please try again");
                            id = 1;
                        }
                    }
                    if (id == 3)
                    {
                        Pizza price = new Pizza();
                        price.PepperoniPrice();
                        Console.WriteLine("Please enter small to order a small pizza or large to order a large pizza");
                        var size = Console.ReadLine();
                        size = size.ToLower();
                        Console.Clear();
                        if (size == "small")
                        {
                            var smallprice = 5;
                            order.Add("Pepperoni", smallprice);
                            Console.WriteLine("Do you want to order another pizza? Y/N");
                            select = Console.ReadLine();
                            select = select.ToLower();
                            Console.Clear();
                        }
                        if (size == "large")
                        {
                            var lgeprice = 8;
                            order.Add("Pepperoni", lgeprice);
                            Console.WriteLine("Do you want to order another pizza? Y/N");
                            select = Console.ReadLine();
                            select = select.ToLower();
                            Console.Clear();
                        }
                        if ((size != "small") && (size != "large"))
                        {
                            Console.WriteLine("You need to enter small or large when choosing size. Please try again");
                            id = 1;
                        }

                }            
                

                } while (select == "y");



            return order;            
               
                            
        }
        public static List<Tuple<string, double>> Drinkorder()
        {
            var id = 0;
            var select = "y";
            do
            {
                Drinks drinklist = new Drinks();
                drinklist.DrinkType();
                Console.WriteLine($"Please 1 to add Coke, 2 to add Coke Zero or 3 to add Juice");
                
                string idchk = Console.ReadLine();
                int x;
                while (!Int32.TryParse(idchk, out x))
                {
                    Console.WriteLine("Not a valid option, please enter the quantity required.");

                    idchk = Console.ReadLine();
                }
                id = x;
                Console.Clear();
                if (id == 1)
                {
                    Drinks price = new Drinks();
                    price.Coke();
                    int quant = 0;
                    Console.WriteLine("Please enter the quantity required");
                    
                    string quantchk = Console.ReadLine();
                    int xquant;
                    while (!Int32.TryParse(quantchk, out xquant))
                    {
                        Console.WriteLine("Not a valid option, please enter the quantity required.");

                        quantchk = Console.ReadLine();
                    }
                    quant = xquant;
                    var i = 0;
                    while(i < quant)
                    {
                        drink.Add(Tuple.Create("Coke", 2.50));
                        i++;
                    }
                    Console.WriteLine("Do you want to order another drink? Y/N");
                    select = Console.ReadLine();
                    select = select.ToLower();
                    Console.Clear();

                    }
                if (id == 2)
                {
                    Drinks price = new Drinks();
                    price.CokeZero();
                    Console.WriteLine("Please enter the quantity required");
                    int quant = 0;

                    string quantchk = Console.ReadLine();
                    int xquant;
                    while (!Int32.TryParse(quantchk, out xquant))
                    {
                        Console.WriteLine("Not a valid option, please enter the quantity required.");

                        quantchk = Console.ReadLine();
                    }
                    quant = xquant;
                    var i = 0;
                    while (i < quant)
                    {
                        drink.Add(Tuple.Create("Coke Zero", 2.50));
                        i++;
                    }
                    Console.WriteLine("Do you want to order another drink? Y/N");
                    select = Console.ReadLine();
                    select = select.ToLower();
                    Console.Clear();
                }
                if (id == 3)
                {
                    Drinks price = new Drinks();
                    price.Juice();
                    Console.WriteLine("Please enter the quantity required");
                    int quant = 0;

                    string quantchk = Console.ReadLine();
                    int xquant;
                    while (!Int32.TryParse(quantchk, out xquant))
                    {
                        Console.WriteLine("Not a valid option, please enter the quantity required.");

                        quantchk = Console.ReadLine();
                    }
                    quant = xquant;
                    var i = 0;
                    while (i < quant)
                    {
                        drink.Add(Tuple.Create("Juice", 3.50));
                        i++; 
                    }
                    Console.WriteLine("Do you want to order another drink? Y/N");
                    select = Console.ReadLine();
                    select = select.ToLower();
                    Console.Clear();

                }
                
             } while (select == "y");

            return drink;
        }

        public static void Checkdetail()
        {
            Console.WriteLine($"Please check your details are correct. Enter C to change or press enter to continue");
            ConsoleKeyInfo select = Console.ReadKey();
            if (select.KeyChar == 'c')
            {
                Console.WriteLine("");
                string name = CustomerName();
                string phone = CustomerPhone();
                Customer changecust = new Customer(name, phone);
                Console.Clear();
                Console.WriteLine("Thank you for updating your details. You will now be taken to the checkout");
            }
            else if (select.Key == ConsoleKey.Enter)
            {
                Console.Clear();
                Console.WriteLine("Thank you for checking your details. You will now be taken to the checkout");
            }

        }

        public static void Payment()
        {
            double pizzaprice = order.Sum(x => x.Value);
            double drinkprice = drink.Sum(x => x.Item2);
            double total = pizzaprice + drinkprice;
            double payment = 0;
            Console.WriteLine($"The total price of your order is ${total}.");
            Console.WriteLine($"Please enter the total amount to be paid");
            
            
            string paychk = Console.ReadLine();
            int xpay;
            while (!Int32.TryParse(paychk, out xpay))
            {
                Console.WriteLine("Not a valid option, please enter the quantity required.");

                paychk = Console.ReadLine();
            }
            payment = xpay;
            double change = payment - total;
            Console.Clear();
            Console.WriteLine($"Your change is ${change}. Thank you for your order");

        }
        
    }
}

